var Builder = require('creep.builder');
var Harvester = require('creep.harvester');
var Upgrader = require('creep.upgrader');
class Creeps {

    constructor() {
        this.roles = {
            'harvester': new Harvester(),
            'upgrader': new Upgrader(),
            'builder': new Builder()
        };
    }

    run() {
        console.log('== Creep Main Loop ==');
        this.cleanupCreeps();
        this.processCreeps();
        this.spawnCreeps();
        console.log('====\n\n');
    }

    cleanupCreeps() {
        // clear dead creep memory
        for (var name in Memory.creeps) {
            if (!Game.creeps[name]) {
                Memory.creeps[name] = undefined;
            }
        }
    }

    processCreeps() {
        console.log('-- Process Creeps');
        for (let name in Game.creeps) {
            var creep = Game.creeps[name];

            this.roles[creep.memory.role].run(creep);
        }
    }

    spawnCreeps() {
        console.log('-- Spawn Creeps');
        var numHaversters = _.sum(Game.creeps, (c) => c.memory.role == 'harvester');
        var numUpgraders = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader');
        var numBuilders = _.sum(Game.creeps, (c) => c.memory.role == 'builder');

        console.log('Harvesters: ', numHaversters);
        console.log('Upgraders: ', numUpgraders);
        console.log('Builders: ', numBuilders);


        if (numHaversters < this.roles['harvester'].minimum) {
            var creep = this.roles['harvester'];
            var name = creep.spawn(Game.spawns.Spawn1);
        } else if (numUpgraders < this.roles['upgrader'].minimum) {
            console.log('Spawning Upgrader');
            var creep = this.roles['upgrader'];
            var name = creep.spawn(Game.spawns.Spawn1);
            console.log('name', name);
        } else if (numBuilders < this.roles['builder'].minimum) {
            var creep = this.roles['builder'];
            var name = creep.spawn(Game.spawns.Spawn1);
        } else {
            // var creep = this.roles['harvester'];
            // var name = creep.spawn(Game.spawns.Spawn1);
        }
    }

}

module.exports = Creeps;
