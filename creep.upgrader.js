var Harvester = require('creep.harvester');

class Upgrader extends Harvester{
    constructor() {
        super();
        this.role = 'upgrader';
        this.body = [WORK, CARRY, MOVE, MOVE];
        this.minimum = 10;
    }

    run(creep) {
        this.setState(creep);

        if (creep.memory.working == true) {
            this.upgrade(creep);
        } else {
            this.harvest(creep);
        }
    }

    upgrade(creep) {
        this.doAction(creep, creep.upgradeController, creep.room.controller);
    }

    setState(creep) {
        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
        }
    }
}

module.exports = Upgrader;
