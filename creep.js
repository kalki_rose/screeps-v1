class Creep {
    constructor() {
    }

    doAction(creep, action, target) {
        if (action.call(creep, target) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target);
        }
    }

    findTarget(creep, type, filter = (t) => t ) {
        return creep.pos.findClosestByPath(type, {filter: filter});
        // return creep.pos.findClosestByPath(type, filter);
    }

    findShortestPath(origin, target) {
        console.log('Path origin:', origin);
        console.log('Path Target:', target);

        let end = {
            pos: target.pos,
            range:1
        }

        let ret = PathFinder.search(
            origin.pos,
            end,
            {
                // We need to set the defaults costs higher so that we
                // can set the road cost lower in `roomCallback`
                plainCost: 1,
                swampCost: 1,

                // roomCallback: function(roomName) {
                //
                //     let room = Game.rooms[roomName];
                //     // In this example `room` will always exist, but since PathFinder
                //     // supports searches which span multiple rooms you should be careful!
                //     if (!room) return;
                //     let costs = new PathFinder.CostMatrix;
                //
                //     Room.find(FIND_STRUCTURES).forEach(function(structure) {
                //         if (structure.structureType === STRUCTURE_ROAD) {
                //             // Favor roads over plain tiles
                //             costs.set(structure.pos.x, structure.pos.y, 10);
                //         }
                //     });
                //
                //     return costs;
                // }
            }
        );

        return ret;
    }

    spawn(base) {
        // console.log('Spawning: ', this.role);

        var parts = this.body.length;
        var energy = Game.spawns.Spawn1.room.energyCapacityAvailable;
        var loops = Math.floor(energy / (parts * 50));

        // console.log('Energy Capacity: ', energy);
        // console.log('Loop Count: ', loops);
        var body = [];
        for (let part of this.body) {
            for(let i = 0; i < loops; i++) {
                body.push(part);
            }
        }

        // console.log(body);

        var creep =  base.createCreep(body, undefined, {
            role: this.role,
            working: false
        });

        if (creep < 0) {
            creep =  base.createCreep(this.body, undefined, {
                role: this.role,
                working: false
            });
        }

        return creep;
    }

    // spawn(base) {
    //     return super.spawn(base, this.body, this.role);
    // }

    getRole() {
        return this.role;
    }

    getMinimum() {
        return this.minimum;
    }
}

module.exports = Creep;
