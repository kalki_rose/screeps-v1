var Creep = require('creep');

class Harvester extends Creep {
    constructor() {
        super();
        this.role = 'harvester';
        this.body = [WORK, CARRY, MOVE, MOVE];
        this.minimum = 7;
    }

    run(creep) {
        this.setState(creep);

        if (creep.memory.working == true) {
            this.storeEnergy(creep);
        } else {
            this.harvest(creep);
        }
    }

    setState(creep) {
        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
        }
    }

    storeEnergy(creep) {
        var target = this.findTarget(creep, FIND_MY_STRUCTURES, (s) => s.energy < s.energyCapacity);

        if (!target) {
            target = this.findTarget(creep, FIND_MY_STRUCTURES, s => s.energy == s.energyCapacity);
        }

        this.doAction(creep, t => creep.transfer(t, RESOURCE_ENERGY), target);
    }

    harvest(creep) {
        let target = this.findTarget(creep, FIND_SOURCES);
        this.doAction(creep, t => creep.harvest(t), target);
    }
}

module.exports = Harvester;
