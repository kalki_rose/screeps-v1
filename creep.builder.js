var Creep = require('creep');

class Builder extends Creep {
    constructor() {
        super();
        this.role = 'builder';
        this.body = [WORK, CARRY, MOVE, MOVE];
        this.minimum = 4;
    }

    run(creep) {
        // if (!creep.memory.roads) {
        //     creep.memory.roads = 0;
        // }

        // if( creep.memory.roads < 2) {
            // let target = super.findTarget(creep, FIND_SOURCES);
            // console.log('Target: ', target);
            //
            // var path = super.findShortestPath(Game.spawns.Spawn1, target);
            //
            // console.log(path.path[0]);
            //
            // path.path.forEach(function(location) {
            //     creep.room.createConstructionSite(location, STRUCTURE_ROAD);
            // });
        // }

        if (creep.memory.working == true && creep.carry.energy == 0) {
            creep.memory.working = false;
        } else if (creep.memory.working == false && creep.carry.energy == creep.carryCapacity) {
            creep.memory.working = true;
        }

        if (creep.memory.working == true) {
            let target = super.findTarget(creep, FIND_CONSTRUCTION_SITES);

            if (target) {
                this.doAction(creep, t => creep.build(t), target);
            } else {
                let target = super.findTarget(creep, FIND_STRUCTURES, s => s.hits < s.hitsMax / 2 && s.structureType != STRUCTURE_WALL);
                this.doAction(creep, t => creep.repair(t), target);
            }
        } else {
            let target = this.findTarget(creep, FIND_SOURCES);
            this.doAction(creep, t => creep.harvest(t), target);
        }
    }

}

module.exports = Builder;
